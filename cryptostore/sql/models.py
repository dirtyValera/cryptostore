# https://stackoverflow.com/questions/7300948/add-column-to-sqlalchemy-table for migrations

from sqlalchemy import create_engine, Column, String, JSON, DateTime, func
from sqlalchemy.orm import declarative_base, sessionmaker
from cryptofeed.defines import TICKER, TRADES, L1_BOOK, L2_BOOK, L3_BOOK, LIQUIDATIONS, OPEN_INTEREST, FUNDING, INDEX, CANDLES
import os

Base = declarative_base()
Session = sessionmaker()

# TODO factor out to common/utils and reuse
VALID_CHANNELS = [TICKER, TRADES, L1_BOOK, L2_BOOK, L3_BOOK, LIQUIDATIONS, OPEN_INTEREST, FUNDING, INDEX, CANDLES]

class DataFeedConfig(Base):
    __tablename__ = 'data_feed_configs'
    version = Column(String(40), primary_key=True)
    config = Column(JSON)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
    exchange = Column(String(40))
    instrument_type = Column(String(40))
    instrument_extra = Column(JSON)
    channels = Column(JSON)
    symbols = Column(JSON)

    def __repr__(self):
        return "<DataFeedConfig(version='%s', config='%s')>" % (self.version, self.config)


class MysqlClient:
    def __init__(self, config):
        self.config = config
        self.engine = self._init_engine()

    def _init_engine(self):
        # TODO figure out users/priviliges
        user = os.getenv('MYSQL_USER')
        password = os.getenv('MYSQL_PASSWORD')
        host = os.getenv('MYSQL_HOST')
        port = os.getenv('MYSQL_PORT')
        db = os.getenv('MYSQL_DATABASE')
        url = f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}'
        engine = create_engine(url, echo=True)
        Session.configure(bind=engine)
        return engine

    def create_tables(self):
        # creates if not exists
        Base.metadata.create_all(self.engine)

    def write_config(self):
        parsed_config = self._parse_config()
        dfc = DataFeedConfig(
            version=parsed_config['version'],
            exchange=parsed_config['exchange'],
            instrument_type=parsed_config['instrument_type'],
            instrument_extra=parsed_config['instrument_extra'],
            channels=parsed_config['channels'],
            symbols=parsed_config['symbols'],
            config=self.config
        )
        session = Session()
        session.merge(dfc)
        session.commit()

    def _parse_config(self):
        # TODO handle exception for config parsing
        version = self.config['svoe']['version']
        instrument_type = self.config['svoe']['instrument_type']
        instrument_extra = {} # TODO instrument_extra
        exchange = list(self.config['exchanges'].keys())[0] # first exchange
        channels = list(set(self.config['exchanges'][exchange].keys()) & set(VALID_CHANNELS)) # filter existing channels
        all_symbols = []
        for channel in channels:
            if channel in [L2_BOOK, L3_BOOK]:
                symbols = self.config['exchanges'][exchange][channel]['symbols']
            else:
                symbols = self.config['exchanges'][exchange][channel]
            for symbol in symbols:
                if symbol not in all_symbols:
                    all_symbols.append(symbol)

        return {
            'version': version,
            'exchange': exchange,
            'instrument_type': instrument_type,
            'instrument_extra': instrument_extra,
            'channels': channels,
            'symbols': all_symbols
        }
