import json
import threading
import logging
import os

from http.server import HTTPServer, BaseHTTPRequestHandler

LOG = logging.getLogger('cryptostore')

# TODO export Prometheus metrics
class HealthCheck:
    def __init__(self, spawner, aggregator, shared_health_status, port, path):
        self.spawner = spawner
        self.aggregator = aggregator
        self.shared_health_status = shared_health_status
        self.port = port
        self.path = path

    def run(self):
        self._start_server()
        LOG.info(f'Health server started on port {self.port}, path {self.path}')

    def _start_server(self):
        server_address = ('', self.port)

        def handler(*args):
            _RequestHandler(self.spawner, self.aggregator, self.shared_health_status, self.path, *args)

        self.httpd = HTTPServer(server_address, handler)
        t = threading.Thread(target=self.httpd.serve_forever)
        t.daemon = True
        t.start()

    def stop(self):
        LOG.info('Shutting down Health server...')
        if self.httpd:
            self.httpd.shutdown()
        LOG.info('Health server stopped')

class _RequestHandler(BaseHTTPRequestHandler):
    def __init__(self, spawner, aggregator, shared_health_status, health_path, *args):
        self.spawner = spawner
        self.aggregator = aggregator
        self.shared_health_status = shared_health_status
        self.health_path = health_path
        BaseHTTPRequestHandler.__init__(self, *args)

    def do_GET(self):
        if self.path == self.health_path:
            LOG.info('Checking health...')
            ok, msg = self._check_health()
            status = 200 if ok else 500
            msg = json.dumps(msg).encode(encoding='utf_8')
            self.send_response(status)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(msg)
            LOG.info(f'Health status: {msg}')

    def _check_health(self):
        msg = {}
        msg['Spawner is OK'] = self.spawner.is_alive()
        msg['Aggregator is OK'] = self.aggregator.is_alive()
        for exchange in self.shared_health_status:
            msg[f'Collector for {exchange} is OK'] = self.shared_health_status[exchange]

        # More custom logic can be added here, e.g. check each FeedHandler object for each Collector
        # TODO check the number of runnig collectors is the same as in config
        # TODO check channels/data_types/symbols per collector is same as configured
        for ok in msg.values():
            if not ok:
                return (False, msg)

        return (True, msg)
