
# TODO add to setup.py requirements
import awswrangler as wr
import pandas as pd
import boto3
import os
import json

from cryptostore.data.store import Store
from cryptofeed.symbols import Symbol, str_to_symbol

# TODO dashboard stuff on streamlit
class SvoeStore(Store):

    def __init__(self, config):
        self.bucket = config['s3_bucket']
        self.prefix = config['s3_prefix']
        self.database = config['glue_database']
        version = config['version']
        # TODO figure out config version logging for testing
        if os.getenv('ENV') == 'TESTING':
            version = 'testing-' + str(config['version'])
        self.version = version
        self.compression = config['compression']
        self.boto3_session = boto3.Session(
            aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
            aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
            region_name=os.getenv('AWS_REGION')
        )

    def aggregate(self, data):
        if isinstance(data[0], dict):
            # Case `data` is a list or tuple of dict.
            self.data = pd.DataFrame(data)
        else:
            # Case `data` is a tuple with tuple of keys of dict as 1st parameter,
            # and generator of dicts as 2nd paramter.
            # DataFrame creation is faster by more than 10% if column names are provided.
            self.data = pd.DataFrame(data[1], columns=data[0])
    # TODO rename data_type -> channel
    def write(self, exchange, data_type, pair, timestamp):
        if self.data.empty:
            # TODO log?
            # TODO add dummy file? If not some alerts on s3 may fire as no data is written in a fixed timeframe
            # TODO there is a metric upstream (in aggregator) tracking block size, writing 0 if empty
            return

        df = self.data
        df['version'] = self.version
        df['date'] = pd.to_datetime(df['receipt_timestamp'], unit='s').dt.strftime('%Y-%m-%d')
        first_timestamp = df['receipt_timestamp'].iloc[0]
        last_timestamp = df['receipt_timestamp'].iloc[-1]

        df['exchange'] = exchange
        df['symbol'] = pair # TODO rename pair to symbol

        # parse base, quote and instrument type
        symbol = str_to_symbol(pair)
        df['quote'] = symbol.quote
        df['base'] = symbol.base
        df['instrument_type'] = symbol.type
        df['instrument_extra'] = json.dumps({}) # TODO instrument_extra

        # indicate as raw uncompacted data
        df['compaction'] = 'raw'

        # TODO check if date > 1 (i.e. this block has two days overlap) and make separate writes
        # TODO or check if wrangler already does it
        wr.s3.to_parquet(
            df=df,
            path=f's3://{self.bucket}/{self.prefix}/{data_type}',
            compression=self.compression,
            dataset=True,
            filename_prefix=self._filename_prefix(exchange, data_type, pair, first_timestamp, last_timestamp),
            partition_cols=[
                'exchange',
                'instrument_type',
                'instrument_extra',
                'symbol',
                'base',
                'quote',
                'date',
                'compaction',
                'version',
            ],
            schema_evolution=True,
            database=self.database,
            table=self._athena_table_name(data_type),
            boto3_session=self.boto3_session
        )

    @staticmethod
    def _filename_prefix(exchange, data_type, pair, first_timestamp, last_timestamp):
        # delimeter = '*'
        return f'{exchange}*{data_type}*{pair}*{first_timestamp}*{last_timestamp}*'

    @staticmethod
    def _athena_table_name(data_type):
        # TODO use TypedDataFrame to validate schemas for each data_type
        # TODO https://github.com/areshytko/typedframe
        # each data_type has different schema, hence separate table
        return data_type

