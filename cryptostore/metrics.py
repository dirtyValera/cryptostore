import os
import logging
import glob

from prometheus_client import Gauge, Counter, Histogram, multiprocess, REGISTRY, start_http_server

LOG = logging.getLogger('cryptostore')

def _setup_multiproc_prometheus(multiproc_dir):
    # create if doesnt exist
    if not os.path.exists(multiproc_dir):
        os.mkdir(multiproc_dir)
        LOG.info(f'Created prometheus multiproc dir: {multiproc_dir}')
    else:
        LOG.info(f'Prometheus multiproc dir already exists: {multiproc_dir}')

    # clear dir
    files = glob.glob(multiproc_dir + '/*')
    for f in files:
        os.remove(f)

    if 'PROMETHEUS_MULTIPROC_DIR' not in os.environ:
        raise Exception('PROMETHEUS_MULTIPROC_DIR is not set')
        # os.environ['PROMETHEUS_MULTIPROC_DIR'] = '/Users/anov/IdeaProjects/svoe/prometheus_multiproc_dir' # TODO
    else:
        dir = os.environ['PROMETHEUS_MULTIPROC_DIR']
        LOG.info(f'PROMETHEUS_MULTIPROC_DIR is: {dir}')

    multiprocess.MultiProcessCollector(REGISTRY)

def start_prometheus(config):
    port = config['prometheus']['port']
    multiproc_dir = config['prometheus']['multiproc_dir']
    _setup_multiproc_prometheus(multiproc_dir)
    start_http_server(port)
    LOG.info("Prometehus started on port %d", port)

# TODO 'symbol' is high cardinality, do we need latency per symbol? Theoretically it should differ per channel
# Latency
LATENCY_LABELS = ['operation', 'exchange', 'data_type', 'symbol'] # TODO add version label/exemplar
LATENCY_HISTOGRAM = Histogram('svoe_data_feed_aggregator_latency_s_histogram', 'Svoe Data Feed Aggreagtor Latency Histogram', LATENCY_LABELS)

# Block Size
CACHED_BLOCK_SIZE_LABELS = ['exchange', 'data_type', 'symbol'] # TODO add version label/exemplar

# TODO should be Summary?
CACHED_BLOCK_SIZE_GAUGE = Gauge('svoe_data_feed_aggregator_cached_block_size_kb_gauge', 'Svoe Data Feed Aggreagtor Cached Block Size Gauge', CACHED_BLOCK_SIZE_LABELS)

def log_latency(delta, *labelvalues):
    LATENCY_HISTOGRAM.labels(*labelvalues).observe(delta)

def log_block_size(size, *labelvalues):
    CACHED_BLOCK_SIZE_GAUGE.labels(*labelvalues).set(size)

# TODO prom mark_process_dead for each process, is this needed??
# TODO log aggregator write retry counts
# TODO add version/config hash as a label